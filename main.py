import requests
import datetime
import urllib3
import aiohttp
import asyncio
import time

async def create_booking(booking, session):
    async with session.post(f"{api_url}/bookings", json=booking, ssl=False) as response:
        resp = await response.json()

        return resp["succeeded"] == True or resp["errorCode"] in [4050, 4069]


async def create_bookings(bookings, day):
    async with aiohttp.ClientSession(trust_env=True) as session:
        start_time = time.time()
        ret = await asyncio.gather(*[create_booking(booking, session) for booking in bookings])
        print(f"Finalized day {day.strftime('%d. %m. %Y')}. Created {ret.count(True)} bookings in {time.time() - start_time}s")

        return ret.count(True)


def random_booking(date, time, phone_index):
    return {
        "clinicCenterId": clinic_id,
        "bookingDate": date,
        "bookingTime": time[0:5],
        "scheduleType": 0,
        "phone": f"+4219{phone_index:08d}",
        "firstName": "Popici",
        "lastName": "Bezpecnost",
        "birthDay": datetime.datetime.utcnow().isoformat(),
        "email": "jebaSecurity@kokoti.com",
        "bloodType": 0,
        "donorType": 0
    }

urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)
api_url = "https://localhost:4634/api"

clinics_request = requests.get(f"{api_url}/clinicCenter/all", verify=False)
if clinics_request.status_code != 200:
    exit()

clinics = clinics_request.json()["items"]
if len(clinics) == 0:
    exit()

for i, clinic in enumerate(clinics):
    clinic_name = clinic["name"]
    print(f"{i} ... {clinic_name}")

clinic_index = int(input(f"Pick clinic number (0 to {len(clinics) - 1}): "))
clinic_id = clinics[clinic_index]["id"]

days_to_fill = int(input(f"How many days to fill: "))

filled_days = 0
phone_nr = 0
bookings_created = 0
day = datetime.datetime.now()

start_time = time.time()

while filled_days != days_to_fill:
    timeslots_request = requests.get(
        f"{api_url}/clinicCenter/{clinic_id}/timeslotsWithBookings?Type=0&Date={day.isoformat()}", verify=False)

    if timeslots_request.status_code != 200:
        day += datetime.timedelta(days=1)
        continue

    timeslots = timeslots_request.json()["item"]["timeslots"]
    bookings = []
    for slot in timeslots:
        if slot["activeSlotsCount"] == 0:
            continue

        timeslot_bookings = [random_booking(day.isoformat(), slot["from"], phone_nr + i) for i in
                             range(slot["activeSlotsCount"])]
        phone_nr += len(timeslot_bookings)
        bookings.extend(timeslot_bookings)

    created = asyncio.get_event_loop().run_until_complete(create_bookings(bookings, day))

    bookings_created += created
    filled_days += 1

    day += datetime.timedelta(days=1)

print(f"Created {bookings_created} in {time.time() - start_time}s")